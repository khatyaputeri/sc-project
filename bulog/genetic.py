import random

total = 0
needs = {}
path_cost = {"gp-t1": 50, "t1-t2": 20, "be-gp": 40, "gp-jm": 22, "be-bo": 75, "be-jm": 70, \
             "bo-jm": 57, "jm-t2": 55, "gp-t2": 55, "bo-t2": 50, "be-t1": 115, "jm-t1": 60,
             "bo-t1": 55, "be-t2": 120}
population = []
candidates = []
candidates_score = 0;


def set_up(total, needs):
    total = total
    needs = needs
    path_cost = {}
    population = ["gp-jm-t1-t2-bo-be-gp", "gp-jm-t1-gp", "gp-t1-t2-jm-bo-be-gp",
                  "gp-t1-t2-be-gp", "gp-jm-t1-bo-be-t2-gp", "gp-t1-jm-bo-be-t2-gp",
                  "gp-bo-be-gp", "gp-t1-t2-be-bo-gp", "gp-t2-be-gp", "gp-jm-bo-gp", "gp-t1-gp",
                  "gp-t2-gp", "gp-bo-gp", "gp-be-gp", "gp-jm-gp"]
    best_value = 1
    all_best_ind = []

    best_ind, best_remain, population = make_new_population(population, total, needs)

    all_best_ind.append(best_ind)
    best_value = best_remain

    counter = 10
    while (counter > 0):
        best_ind, best_remain, population = make_new_population(population, total, needs)
        if (best_remain < best_value):
            all_best_ind = []
            all_best_ind.append(best_ind)
            best_value = best_remain
        elif (best_remain == best_value):
            all_best_ind.append(best_ind)
        counter -= 1

    # cari jalan paling pendek
    if (len(all_best_ind) > 0):
        val = count_path_cost(all_best_ind[0])
        ind = all_best_ind[0]

        for i in (all_best_ind):
            tmp = count_path_cost(i)
            if (tmp < val):
                ind = i
                val = tmp

    # buang kalau ada yang dilewati tapi 0
    points = ind.split("-")
    final_inds = []
    for i in (points):
        if (i != 'gp'):
            if (needs[i] != 0):
                final_inds.append(i)
        else:
            final_inds.append(i)

    ind = "-".join(final_inds)

    return ind, best_value


# digunakan untuk menentukan individu terbaik dengan remaining sama
def count_path_cost(path):
    # print("hehe")
    points = path.split("-")
    cost = 0;
    for i in range(len(points) - 1):
        path_name = points[i] + "-" + points[i + 1] if points[i] < points[i + 1] else points[
                                                                                          i + 1] + "-" + \
                                                                                      points[i]
        if path_name in path_cost:
            cost += path_cost[path_name]
        # else:
        #     return -1;

    return cost


# digunakan sebagai fitness function 1
def count_remaining(path, total, needs):
    total_needs = 0
    points = path.split("-")
    points_sets = set(points)

    for point in points_sets:
        if (point in needs):
            total_needs += needs[point]

    remaining = total - total_needs

    return remaining


def tournament(path1, path2):
    rem1 = count_remaining(path1)
    rem2 = count_remaining(path2)

    if (rem1 == rem2):
        cost1 = count_path_cost(path1)
        cost2 = count_path_cost(path2)

        if (cost1 < cost2):
            return path1

        return path2

    if rem1 < rem2:
        return path1

    return path2


def find_best(population, total, needs):
    best_cost = 1
    best_ind = population[0]

    for ind in population:
        cost = fitness_function(ind, total, needs)
        if (cost >= 0):
            if (cost < best_cost):
                best_cost = cost
                best_ind = ind

    return best_ind, best_cost, population


def make_new_population(population, total, needs):
    new_population = []
    for i in range(len(population)):
        parent1 = random.choice(population)
        parent2 = random.choice(population)

        child1, child2 = reproduce(parent1, parent2)

        new_population.append(child1)
        new_population.append(child2)

    population = new_population

    return find_best(population, total, needs)


def reproduce(parent1, parent2):
    lst_path1 = parent1.split("-")
    len1 = len(lst_path1)

    lst_path2 = parent2.split("-")
    len2 = len(lst_path2)

    len_min = 0
    len_max = 0

    if (len1 < len2):
        len_min = len1
        len_max = len2
    else:
        len_min = len2
        len_max = len1

    selection_point = random.randint(1, len_min - 1)

    individu1 = []
    individu2 = []

    for i in range(len_max):
        if (i < selection_point):
            individu1.append(lst_path1[i])
            individu2.append(lst_path2[i])
        else:
            if (i < len2):
                individu1.append(lst_path2[i])
            if (i < len1):
                individu2.append(lst_path1[i])

    ind1 = "-".join(individu1)
    ind2 = "-".join(individu2)

    return [ind1, ind2]


def fitness_function(path, total, needs):
    remaining = count_remaining(path, total, needs)
    return remaining / total
