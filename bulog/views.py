from django.shortcuts import render
from .genetic import set_up
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse


# Create your views here.
def index(request):
    response = {}
    return render(request, 'bulog.html', response)


# input total : integer bawaan truk
# needs : dictionary, key string inisial nama gudang, value integer kebutuhan gudang
# return list of list pasangan gudang
def get_path(total, needs):
    path, fitness = set_up(total, needs)
    sisa = int(fitness * total)
    return get_path_list(path), sisa


def get_path_list(path):
    points = path.split("-")
    ret_arr = []
    for i in range(len(points) - 1):
        tmp = [points[i], points[i + 1]]
        ret_arr.append(tmp)

    return ret_arr


@csrf_exempt
def get_initial_path(request):
    gp = request.POST.get('gp')
    t1 = request.POST.get('t1')
    t2 = request.POST.get('t2')
    bo = request.POST.get('bo')
    be = request.POST.get('be')
    jm = request.POST.get('jm')
    result, sisa = get_path(int(gp), {'jm': int(jm), 't1': int(t1), 't2': int(t2),
                            'bo': int(bo),
                            'be': int(be)})
    best_search = {'res': result, 'sisa' : sisa}
    return JsonResponse(best_search)
