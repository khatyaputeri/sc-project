from django.conf.urls import url
from .views import index, get_initial_path

# url for app, add your URL Configuration

urlpatterns = [
    # TODO Implement this
    url(r'^$', index, name='index'),
    url(r'^initial_path/$', get_initial_path, name='initial_path/')
]
