$(document).ready(function(){
    $(".box").hide();
    $(".gp-t1-shadow").hide();
    $(".t1-t2-shadow").hide();
    $(".t2-bo-shadow").hide();
    $(".bo-be-shadow").hide();
    $(".be-gp-shadow").hide();
    $(".gp-jm-shadow").hide();
    $(".jm-bo-shadow").hide();
    $(".gp-t2-shadow").hide();
    $(".gp-bo-shadow").hide();
    $(".t2-jm-shadow").hide();
    $(".jm-bo-shadow").hide();
    $(".jm-be-shadow").hide();
    $(".t1-be-shadow").hide();
    $(".t1-be-1-shadow").hide();
    $(".t1-jm-shadow").hide();
    $(".t1-bo-shadow").hide();
    $(".t1-bo-1-shadow").hide();
    $(".t2-be-shadow").hide();
    $(".t2-be-1-shadow").hide();
    $(".gp").click(function(){
        if ($('#gp-node').is(":hidden")){
          $("#gp-node").show();
        }else{
          $("#gp-node").hide();
        }
    });
    $(".t1").click(function(){
        if ($('#t1-node').is(":hidden")){
          $("#t1-node").show();
        }else{
          $("#t1-node").hide();
        }
    });
    $(".t2").click(function(){
        if ($('#t2-node').is(":hidden")){
          $("#t2-node").show();
        }else{
          $("#t2-node").hide();
        }
    });
    $(".bo").click(function(){
        if ($('#bo-node').is(":hidden")){
          $("#bo-node").show();
        }else{
          $("#bo-node").hide();
        }
    });
    $(".be").click(function(){
        if ($('#be-node').is(":hidden")){
          $("#be-node").show();
        }else{
          $("#be-node").hide();
        }
    });
    $(".jm").click(function(){
        if ($('#jm-node').is(":hidden")){
          $("#jm-node").show();
        }else{
          $("#jm-node").hide();
        }
    });
$(".btn").click(function(){
        $(".gp-t1-shadow").hide();
        $(".t1-t2-shadow").hide();
        $(".t2-bo-shadow").hide();
        $(".bo-be-shadow").hide();
        $(".be-gp-shadow").hide();
        $(".gp-jm-shadow").hide();
        $(".jm-bo-shadow").hide();
        $(".gp-t2-shadow").hide();
        $(".gp-bo-shadow").hide();
        $(".t2-jm-shadow").hide();
        $(".jm-bo-shadow").hide();
        $(".jm-be-shadow").hide();
        $(".t1-be-shadow").hide();
        $(".t1-be-1-shadow").hide();
        $(".t1-jm-shadow").hide();
        $(".t1-bo-shadow").hide();
        $(".t1-bo-1-shadow").hide();
        $(".t2-be-shadow").hide();
        $(".t2-be-1-shadow").hide();

        $('circle').css({fill:"#aec3cf"});
        $('circle').css({stroke:"#aec3cf"});

        $('.res-node').empty();
        $('.sisa').empty();
        $('.res-path').empty();

        var gp = $("#gp-input").val();
        var t1 = $("#t1-input").val();
        var t2 = $("#t2-input").val();
        var bo = $("#bo-input").val();
        var be = $("#be-input").val();
        var jm = $("#jm-input").val();
        if ((gp == '') || (t1 == '') || (t2 == '') || (bo == '') || (be == '') || (jm == '')) {
          alert("Harap isi semua kebutuhan. Jika tidak ada kebutuhan, maka tuliskan 0 secara eksplisit");
        }else{
          if ((parseInt(gp) > 15000) || (parseInt(t1) > 15000) || (parseInt(t2) > 15000) || (parseInt(bo) > 15000) || (parseInt(be) > 15000) || (parseInt(jm) > 15000))  {
            alert('Berat tidak boleh lebih dari 15000 kg');
          }else {
            $.ajax({
                    type: 'POST',
                    data: {gp:gp, t1:t1, t2:t2, bo:bo, be:be, jm:jm},
                    url:'initial_path/', // ganti url
                    dataType: 'json',
                        success: function (json) {
                            $.each( json['res'], function( i, val ) {
                                console.log(val[0]);
                                if ((val[0] == 'gp' && val[1] =='t1') || (val[0]=='t1' &&
                                val[1] == 'gp')){
                                    $(".gp-t1-shadow").show();
                                }
                                if ((val[0] == 't1' && val[1] =='t2') || (val[0]=='t2' &&
                                val[1] == 't1')){
                                    $(".t1-t2-shadow").show();
                                }
                                if ((val[0] == 't2' && val[1] =='bo') || (val[0]=='bo' &&
                                val[1] == 't2')){
                                    $(".t2-bo-shadow").show();
                                }
                                if ((val[0] == 'bo' && val[1] =='be') || (val[0]=='be' &&
                                val[1] == 'bo')){
                                    $(".bo-be-shadow").show();
                                }
                                if ((val[0] == 'be' && val[1] =='gp') || (val[0]=='gp' &&
                                val[1] == 'be')){
                                    $(".be-gp-shadow").show();
                                }
                                if ((val[0] == 'gp' && val[1] =='jm') || (val[0]=='jm' &&
                                val[1] == 'gp')){
                                    $(".gp-jm-shadow").show();
                                }
                                if ((val[0] == 'jm' && val[1] =='bo') || (val[0]=='bo' &&
                                val[1] == 'jm')){
                                    $(".jm-bo-shadow").show();
                                }
                                if ((val[0] == 'gp' && val[1] =='t2') || (val[0]=='t2' &&
                                val[1] == 'gp')){
                                    $(".gp-t2-shadow").show();
                                }
                                if ((val[0] == 'gp' && val[1] =='bo') || (val[0]=='bo' &&
                                val[1] == 'gp')){
                                    $(".gp-bo-shadow").show();
                                }
                                if ((val[0] == 't2' && val[1] =='jm') || (val[0]=='jm' &&
                                val[1] == 't2')){
                                    $(".t2-jm-shadow").show();
                                }
                                if ((val[0] == 'jm' && val[1] =='bo') || (val[0]=='bo' &&
                                val[1] == 'jm')){
                                    $(".jm-bo-shadow").show();
                                }
                                if ((val[0] == 'jm' && val[1] =='be') || (val[0]=='be' &&
                                val[1] == 'jm')){
                                    $(".jm-be-shadow").show();
                                }
                                if ((val[0] == 't1' && val[1] =='be') || (val[0]=='be' &&
                                val[1] == 't1')){
                                    $(".t1-be-shadow").show();
                                    $(".t1-be-1-shadow").show();
                                }
                                if ((val[0] == 't1' && val[1] =='jm') || (val[0]=='jm' &&
                                val[1] == 't1')){
                                    $(".t1-jm-shadow").show();
                                }
                                if ((val[0] == 't1' && val[1] =='bo') || (val[0]=='bo' &&
                                val[1] == 't1')){
                                    $(".t1-bo-shadow").show();
                                    $(".t1-bo-1-shadow").show();
                                }
                                if ((val[0] == 't2' && val[1] =='be') || (val[0]=='be' &&
                                val[1] == 't2')){
                                    $(".t2-be-shadow").show();
                                    $(".t2-be-1-shadow").show();
                                }
                            });
                            var node = [];
                            $.each( json['res'], function( i, val ) {
                                jQuery.each(val, function(j, vals) {
                                    node.push(vals)
                                });
                            });
                            var uniqueNodes = [];
                            var listDictionary= {'gp': 'Gudang Pusat',
                                                 't1': 'Tangerang I',
                                                 't2': 'Tangerang II',
                                                 'bo': 'Bogor',
                                                 'be': 'Bekasi',
                                                 'jm': 'Jakarta-Menteng'
                                                }
                            $.each(node, function(i, el){
                                if($.inArray(el, uniqueNodes) === -1) {
                                    uniqueNodes.push(el)
                                    $('.'+el).css({fill:"#fdfd96"});
                                    $('.'+el).css({stroke:"pink"});
                                    $('.res-node').append('<p>'+listDictionary[el]+'</p>')
                                };
                            });
                            $.each(json['res'], function(i, val){
                                if(val[0] != val[1]){
                                    $('.res-path').append
                                    ('<p>'+listDictionary[val[0]]+
                                    ' - '+listDictionary[val[1]] + '</p>');
                                }
                            });
                            $('.sisa').append('<p>'+json['sisa']+'kg </p')
                            console.log(uniqueNodes);


                  }
            });
          }
        }
    });
  });